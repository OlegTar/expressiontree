﻿using System;

namespace ConsoleApp
{
    public class Student
    {
        public string Name { get; set; }
        public string Test { get; set; }
        public DateTime Date { get; set; }
        public int Mark { get; set; }
    }
}
