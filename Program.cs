﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace ConsoleApp
{
    class Program
    {
        private static List<string> ArgsList;
        private List<string> FlagsList = new List<string>();

        //-name Ivan -minmark 3 -maxmark 5 -datefrom 20/11/2012 -dateto 20/12/2012 -test Maths
        static void Main(string[] args)
        {
            string text = File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Students.json"));
            StudentsCollection students = JsonSerializer.Deserialize<StudentsCollection>(text);

            ParameterExpression student = Expression.Parameter(typeof(Student), "student");
            var expr = Process(WhereFlags.MinMark, "1", student);
            //var expr2 = Process(WhereFlags.Name, "Ivan Petrov", student);
            expr = Expression.And(expr, Process(WhereFlags.Name, "Ivan Petrov", student));

            // Create a lambda expression.  

            Expression<Func<Student, bool>> lambda1 = Expression.Lambda<Func<Student, bool>>(
                expr,
                new[] { student }
            );

            Func<Student, bool> func = lambda1.Compile();
            Console.WriteLine(lambda1);


            //Console.WriteLine(expr);
            //Console.WriteLine(expr2);
            ////Console.WriteLine(be);
           

            //var result1 = students.Students.Where(student2 => student2.Name == "Ivan Petrov" && student2.Mark > 1);
            //foreach (var r in result1)
            //{
            //    Console.WriteLine(r);
            //}

            var result = students.Students.Where(func);
            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
        }

        public static BinaryExpression Process(WhereFlags whereFlags, string value, ParameterExpression param)
        {
            if (whereFlags == WhereFlags.Name)
            {
                MemberExpression propertyName = Expression.PropertyOrField(param, nameof(Student.Name));

                return Expression.Equal(
                    propertyName,
                    Expression.Constant(value, typeof(string)));
            }
            if (whereFlags == WhereFlags.MinMark)
            {
                ParameterExpression studentParamter = Expression.Parameter(typeof(Student), "student");
                MemberExpression propertyMark = Expression.PropertyOrField(param, nameof(Student.Mark));
                return Expression.GreaterThan(
                    propertyMark,
                    Expression.Constant(int.Parse(value), typeof(int)));
            }
            return null;
        }
    }
}
